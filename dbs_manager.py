import psycopg2
#Verwaltet Datenbank-Abfragen

#establish connection
print("Connecting to Database....")
connection = psycopg2.connect(database="DBS", user="postgres", password="Dbs2022Projekt", host="localhost", port=5432)
print("Connected to Database.")
cursor = connection.cursor()


def get_schreibt():#Gibt Tabelle schreibt zurück. (Name,DOI)
    print("Starte DB-Abfrage....")
    cursor.execute("SELECT * from public.schreibt limit 6000;")
    # Fetch all rows from database, stores as list of Tupels
    record = cursor.fetchall()
    print("DB-Abfrage beendet.")
    return record

def get_anz_autor():#Gibt für jedes Paper zurück, wie viele Autoren beteilgt waren, (DOI,COUNT)
    print("Starte DB-Abfrage....")
    cursor.execute("SELECT \"DOI\",COUNT(\"name\") FROM public.schreibt GROUP BY \"DOI\";")
    # Fetch all rows from database, stores as list of Tupels
    record = cursor.fetchall()
    print("DB-Abfrage beendet.")
    return record

def get_anz_paper():#Gibt für jeden Autor zurück, wie viele Artikel er geschriben hat (name,COUNT)
    print("Starte DB-Abfrage....")
    cursor.execute("SELECT \"name\",COUNT(\"DOI\") FROM public.schreibt GROUP BY \"name\";")
    # Fetch all rows from database, stores as list of Tupels
    record = cursor.fetchall()
    print("DB-Abfrage beendet.")
    return record

def close():
    connection.close()
