#Erstellt die Datenpunkte für die Graphen

from plotly.subplots import make_subplots
import plotly.graph_objects as go
import networkx as nx
import dbs_manager as db
import time#performance messung


def autor_tuples(autoren):#Gibt alle Paare an Autoren aus einer Liste mit n Autoren zurück
    liste=[]
    for i in range (len(autoren)):
        for j in range (i+1,len(autoren)):
            liste.append((autoren[i],autoren[j]))
    return liste

#Generiert Traces für Graphen, das Netzwerk der Autoren
def autor_netz_tr():
    start = time.process_time()
    schreibt = db.get_schreibt() #Tabellen Tupel als (Name,DOI)
    

    paper_dict={}#DIO:[Authors]

    g = nx.Graph()
    
    #Erstelle dict mir key=DOI und Wert = [Autor1,Autor2,...]
    for entry in schreibt:
        autor = entry[0]
        doi = entry[1]
        g.add_node(autor)#Füge Autor in Graph ein
        
        if doi in paper_dict:
            paper_dict[doi].append(autor)
        else:
            paper_dict[doi] = [autor]

    #Erstelle Edges aus dem dict
    for paper in paper_dict:
        for autor_pair in autor_tuples(paper_dict[paper]):
            if autor_pair in g.edges:
                g.edges[autor_pair]["weight"]+=1
            else:
                g.add_edge(autor_pair[0],autor_pair[1],weight=1)

    
    print(g)
    print("Dies kann ein wenig dauern...")
    #Erstelle node positions based on springs
    pos = nx.spring_layout(g)
    print("Calculated spring distance")


    ###Erstelle nodes & edge tracey für plotly ###

    #Erstelle node traces, also Datenlisten für die Visualisierung,
    #welche die Positionen der Punkte beinhalten

    node_x=[]
    node_y=[]
    hower_info=[]
    colors=[]

    for node in pos:
        node_x.append(pos[node][0])
        node_y.append(pos[node][1])
        hower_info.append(node)
        colors.append("red")

    #Makiere Daten als Marker, also Punkte, gebe ihnen die x und y Koordinaten und style-informationen
    node_trace = go.Scatter(x=node_x,y=node_y,mode='markers',
                            customdata=hower_info,name="",
                            hovertemplate='Name:%{customdata}',
                            hoverinfo='text')


    #Erstelle edge traces, also Datenlisten für die Visualisierung,
    #welche die Positionen der Kanten beinhalten
    edge_x=[]
    edge_y=[]

    for edge in g.edges():
        x0, y0 = pos[edge[0]]
        x1, y1 = pos[edge[1]]
        edge_x.append(x0)
        edge_x.append(x1)
        edge_x.append(None)
        edge_y.append(y0)
        edge_y.append(y1)
        edge_y.append(None)

    #Makiere Daten als Linies, also Punkte, gebe ihnen die x und y Koordinaten und style-informationen
    edge_trace = go.Scatter(
        x=edge_x, y=edge_y,
        line=dict(width=0.7, color='#888'),
        hoverinfo='none',
        mode='lines')
    print(time.process_time() - start)
    return [node_trace,edge_trace]

def autor_pro_paper_tr():#Generiert Daten für die Datenpunkte für das Diagramm der autoren/paper 
    count = db.get_anz_autor()#[(DOI,COUNT),...]

    count_dict={}
    #Fülle Dict so dass key=Anzahl an Autoren, Wert = Häufigkeit
    for tupel in count:
        cnt= tupel[1]

        if cnt in count_dict:
            count_dict[cnt]+=1
        else:
            count_dict[cnt] = 1

    #Forme daraus die Höhe und Position der Balken
    x_axis=[]
    y_axis=[]
    for key in count_dict:
        x_axis.append(key)
        y_axis.append(count_dict[key])

    #Erstelle Daten formatiert für Balkendiagramm
    bar_trace = go.Bar(
        x=x_axis,
        y=y_axis,
        name="Autoren",
        width=1,
        dx=1,
        x0=1)
    
    
    
    return bar_trace
