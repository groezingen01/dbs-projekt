# DBS-Projekt

## Inhalt
Diese Repo beinhaltet den Code unserer Lösung für den 10. Übungszettel in Datenbanksysteme. 

## Installation
Benötigt als Programme werden:
- ProstgreSQL
- Python

Python benötigt folgende Bibliotheken, welche mit "pip install NAME" installiert werden können:
- plotly
- networkx
- psycopg2

Damit eine Verbindung zur Datenbank aufgebaut werden kann, muss folgendes gegeben sein:
- Ein PostgreSQL server auf localhost:5432
- mit einer Datenbank names "DBS"
- dem User "postgres", mit dem Passwort "Dbs2022Projekt", und entsprechenden Rechten

In der Datenbank muss man zum Unterpunkt "Schemas/Public" navigieren und auf diesen rechtsklicken. In dem Menu wählt man nun "Restore" aus und gibt den Pfad zu "ue10.tar" aus dem Ordner "Daten" diesen Repos an.

Danach sollte ein Doppelklick auf DataVisualisation.py genügen. Die erstellte Visualisierung lässt sich auch in Result finden.

## Resultat
Das Programm generiert eine Webseite mit einer interaktiven Darstellung von den zwei ausgewählten Aspekte aus den Daten. In der Darstellung kann man Zoomen und Filtern navigieren.

![Kollaboration der Autoren](/ReadmeBilder/Im1.png)
![Anzahl der Autoren](/ReadmeBilder/Im2.png)