#!/usr/bin/env python
# coding: utf-8


from plotly.subplots import make_subplots
import plotly.graph_objects as go
import networkx as nx
import dbs_manager as db
import traces as tr
###Erstelle Visualisierung###


#Erstelle Figur aus mehreren Diagrammen
all_fig = make_subplots(rows=2,subplot_titles=('<b>Autorennetz</b>','<b>Wie viele Autoren sitzen an einem Paper?</b>'))#cols=1,row_titles=["F1","F2"])

#Erhalte Diagrammdaten
net_traces = tr.autor_netz_tr()
bar_trace = tr.autor_pro_paper_tr()

#Füge diese in Figur ein
all_fig.add_trace(net_traces[0],row=1,col=1)
all_fig.add_trace(net_traces[1],row=1,col=1)
all_fig.add_trace(bar_trace,row=2,col=1)
all_fig.update_layout(height=1600, width=1000,showlegend=False,title='<br>Analyse der Autorenschaft')


all_fig.show()


db.close()
